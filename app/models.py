# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class TipoDeDocumento(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=64)

    class Meta:
        db_table = 'Tipo de Documento'


class Persona(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo_documento = models.ForeignKey('TipoDeDocumento', on_delete=models.CASCADE)
    nro_documento = models.CharField(unique=True, max_length=16)
    nombre = models.CharField(max_length=64)
    apellido = models.CharField(max_length=64, blank=True, null=True)
    fecha_nacimiento = models.DateField()
    telefono = models.CharField(max_length=32, blank=True, null=True)
    direccion = models.CharField(max_length=128, blank=True, null=True)
    email = models.CharField(max_length=64, blank=True, null=True)
    sexo = models.BooleanField(blank=True, null=True)

    class Meta:
        db_table = 'Persona'


class Usuario(models.Model):
    id = models.IntegerField(primary_key=True)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    fecha_de_registro = models.DateField()
    ultima_conexion = models.DateTimeField(blank=True, null=True)
    area = models.CharField(max_length=64, blank=True, null=True)
    nombre_usuario = models.CharField(max_length=64)
    contrasena = models.CharField(max_length=64)

    class Meta:
        db_table = 'Usuario'
        unique_together = (('persona', 'nombre_usuario'),)


class Servicio(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=128)

    class Meta:
        db_table = 'Servicio'

class Box(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=128)
    servicio = models.ForeignKey('Servicio', on_delete=models.CASCADE)
    estado = models.BooleanField()

    class Meta:
        db_table = 'Box'


class Cliente(models.Model):
    id = models.IntegerField(primary_key=True)
    persona = models.OneToOneField('Persona', on_delete=models.CASCADE)

    class Meta:
        db_table = 'Cliente'


class Cola(models.Model):
    id = models.IntegerField(primary_key=True)
    box = models.OneToOneField(Box, on_delete=models.CASCADE)

    class Meta:
        db_table = 'Cola'

class Prioridad(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=128)

    class Meta:
        db_table = 'Prioridad'

class ColaLista(models.Model):
    id = models.IntegerField(primary_key=True)
    cliente = models.OneToOneField(Cliente, on_delete=models.CASCADE)
    cola = models.ForeignKey(Cola, on_delete=models.CASCADE)
    prioridad = models.ForeignKey('Prioridad', on_delete=models.CASCADE)
    usuario = models.ForeignKey('Usuario', on_delete=models.CASCADE)
    en_espera = models.BooleanField()
    hora = models.TimeField()

    class Meta:
        db_table = 'Cola Lista'

